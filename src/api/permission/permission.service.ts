import { ValidPermission } from "../../data/constant/permission/permission.constant";
import { IPermission } from "src/data/interface/api/permission/permission.interface";
import { Repository } from "typeorm";
import { Permission } from "../../data/entities/api/permission/permision.entity";
import { AppDataSource } from '../../data/database-config/data-source';

export class PermissionService {
  private permissionRepository: Repository<Permission>;
  constructor() {
    this.permissionRepository = AppDataSource.getRepository(Permission);
  }
  async findAll(): Promise<Array<IPermission>> {
    return await this.permissionRepository.find();
  }
  async syncPermissions() {
    const inconsistentPermissions: Array<string> = [];
    console.log("Verifying permissions...");

    let storedPermissions = await this.permissionRepository.find();
    const storedPermissionNames = storedPermissions.map(
      (permission) => permission.permissionName
    );

    const staticPermissions = Object.values(ValidPermission) as Array<string>;


    for (const permission of staticPermissions) {
      if (process.env.DB_SYNCHRONIZE === "true") {
        if (!storedPermissionNames.includes(permission)) {
          await this.permissionRepository.save({
            permissionName: permission,
            permissionDescription: `${permission
              .split(":")
              .join(" ")} (default)`,
            permissionState: true,
          });
        }
      } 
    }

    storedPermissionNames.forEach((permission) => {
      if (!staticPermissions.includes(permission)) {
        inconsistentPermissions.push(permission);
      }
    });

    if (inconsistentPermissions.length > 0) {
      const errorMessage =
        "Inconsistent permissions found: " + inconsistentPermissions.join(",");
      console.error(inconsistentPermissions.join(","));
      throw new Error(errorMessage);
    }
    storedPermissions = await this.permissionRepository.find();
    if (!(storedPermissions.length ===staticPermissions.length)) {
    throw new Error("Permission imcomplite activate sincronized");
    }
    console.log("Verification done!");
  }
}
