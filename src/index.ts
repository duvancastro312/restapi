import AppRouter from './serverConfig/routes';
import { Server } from './serverConfig/server';

async function main() {
  console.log('index process.env.DB_TYPE :>> ',  process.env.DB_TYPE);
  const server = new Server();
  const router = new AppRouter().router();
  const port = process.env.port ? +process.env.port : 3000;
  await server.start({
    port,
    router,
  }).then(() => {

  }).catch(err => {
    console.log('err :>> ', err);
  });
}

void main();
