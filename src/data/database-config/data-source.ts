import { DataSource } from 'typeorm';
import * as dotenv from 'dotenv';
dotenv.config()
console.log('process.env.DB_TYPE  :>> ', process.env.DB_TYPE );
export const AppDataSource = new DataSource({
  type: process.env.DB_TYPE  as
  | 'mysql'
  | 'mssql'
  | 'postgres'
  | 'mongodb',
  host: process.env.DB_HOST,
  port: process.env.DB_PORT ? parseInt(process.env.DB_PORT) : 5432,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  entities: ['**/src/data/entities/**/**/*.entity.{js,ts}'],
  synchronize: process.env.DB_SYNCHRONIZE === 'true', // Convierte el string a booleano
  logging: process.env.DB_LOGGING === 'true', // Convierte el string a booleano
});
