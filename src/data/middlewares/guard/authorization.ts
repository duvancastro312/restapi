import { NextFunction, Response } from "express";
import { User } from "../../entities/api/user/user.entity";
import { AppDataSource } from "../../database-config/data-source";
import { IAuthRequest } from "../../interface/api/jwt/payload.jwt";

export const authorization = (roles: string[]) => {
  return async (req: IAuthRequest, res: Response, next: NextFunction) => {
    const userRepo = AppDataSource.getRepository(User);
    const user = await userRepo.findOne({
      where: { userId: req.currentUser?.userId }, // Accede a currentUser a través de la nueva interfaz AuthRequest
    });
    // esto lo vamos a cambiar para manejarlo por Permission
    if (user && !roles.includes(user.role.roleName)) {
      return res.status(403).json({ message: "Forbidden" });
    }
    next();
  };
};
