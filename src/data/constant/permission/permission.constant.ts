export enum ValidPermission {
  //all
  develop_permission_all = 'develop:permission:all',
  settings_permission_read = 'settings:permission:read',
  //roles
  settings_roles_read = 'settings:roles:read',
  settings_roles_create = 'settings:roles:create',
  settings_roles_update = 'settings:roles:update',
  settings_roles_delete = 'settings:roles:delete',
  //users
  settings_users_read = 'settings:users:read',
  settings_users_create = 'settings:users:create',
  settings_users_update = 'settings:users:update',
  settings_users_delete = 'settings:users:delete',
  //files
  settings_files_read = 'settings:files:read',
  settings_files_create = 'settings:files:create',
  settings_files_update = 'settings:files:update',
  settings_files_delete = 'settings:files:delete',

  //screen
  settings_screen_create = 'settings:screen:create',
  settings_screen_read = 'settings:screen:read',
  settings_screen_update = 'settings:screen:update',
  settings_screen_delete = 'settings:screen:delete',
  //config
  settings_screen_configuration_read = 'settings:screen:configuration:read',
  settings_screen_configuration_create = 'settings:screen:configuration:create',
  settings_screen_configuration_update = 'settings:screen:configuration:update',
  settings_screen_configuration_delete = 'settings:screen:configuration:delete',
  //mosaic
  settings_screen_configuration_mosaic_read = 'settings:screen:configuration:mosaic:read',
  settings_screen_configuration_mosaic_create = 'settings:screen:configuration:mosaic:create',
  settings_screen_configuration_mosaic_update = 'settings:screen:configuration:mosaic:update',
  settings_screen_configuration_mosaic_delete = 'settings:screen:configuration:mosaic:delete',
}
