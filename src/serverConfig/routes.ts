import { Router } from 'express';
import authRoutes from '../api/auth/auth.routes';
import { authentification } from '../data/middlewares/guard/authentification';
import { authorization } from '../data/middlewares/guard/authorization';

export default class AppRouter {
  router() {
    const router = Router();
    router.use('/auth',
    authentification,
    authorization(["admin"]),
     authRoutes);
    // router.use('/users', );
    // router.use('/role', RoleRouter);

    return router;
  }
}
